#SPOTIPPOS - ANÚNCIOS

##Instalação
- Clonar repositório

```
git clone https://wtolotti@bitbucket.org/wtolotti/fe-vivareal-code-challenge.git
```

- Instalando dependências 

```npm install``` ou ```make setup```

Ao finalizar, ele roda também o comando ```bower install``` para dependências do browser

- Erguer server de desenvolvimento

```make server``` ou ```gulp up```

##Build desenvolvimento ( não minificada ) 

- Gerar www

 ```make build``` ou ```gulp build``` 

##Build Liberação ( minificada ) 

- Gerar distribution

```make prod``` ou ```gulp build --compress```

## Deploy para o s3

- Depois de rodar o build minificado, rode o comando abaixo
- URL Prod: http://fe.vivareal.com.s3-website-us-west-2.amazonaws.com

```gulp aws --compress```



## Rotas

### Tela de busca principal
	
	```
		GET /search
	```
### Tela do detalhe do imóvel

	```
		GET /search/:id
	```

