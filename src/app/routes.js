(function() {
	function Routes($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider, $httpProvider, $locationProvider) {
		$stateProvider
			.state('App', {
				abstract: true,
				templateUrl: '/app/views/main/mainTemplate.html',
				controller: 'mainController'
			})
			.state({
				name: 'App.search',
				url: '/search/',
				views: {
					'main@App': {
						templateUrl: '/app/views/search/searchTemplate.html',
						controller: 'searchController'
					}
				}
			})
			.state({
				name: 'App.search.get',
				url: '^/search/:id',
				views: {
					'main@App': {
						templateUrl: '/app/views/properties/propertiesTemplate.html',
						controller: 'propertiesController'
					}
				}
			})

			$urlRouterProvider.rule(function($injector, $location) {

				var path = $location.path();
				var hasTrailingSlash = path[path.length-1] === '/';

				if(hasTrailingSlash) {
					var newPath = path.substr(0, path.length - 1); 
					return newPath; 
				} 

			});

		$urlRouterProvider.otherwise('/search/');
		$locationProvider.html5Mode(true);
	}

	angular.module('app').config(['$stateProvider', '$urlRouterProvider', '$urlMatcherFactoryProvider', '$httpProvider', '$locationProvider', Routes]);

})();