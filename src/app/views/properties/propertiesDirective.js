(function() {
	'use strict';

	function PropertiesDirective() {

		return {
			scope: true,
			restrict: 'E',
			templateUrl: '/app/views/properties/propertiesDirectiveTemplate.html',
		};

	}

	angular.module('app')
		.directive('properties', [PropertiesDirective]);

})();