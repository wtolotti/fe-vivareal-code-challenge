(function() {
	'use strict';

	function PropertiesController(
		$scope,
		$state,
		searchService,
		SEARCH
		) {

		function init() {


			$scope.data = _.clone(SEARCH)
			$scope.properties_detail = true
			$scope.id = $state.params.id
			$scope.loading = true
			$scope.searchList = []

			if(!$scope.id) $state.go('App.search')
			
			search_by_id($scope.id)
		}

		function search_by_id (id){
			var new_data = _.assign($scope.data,{id:id})
			searchService.get_by_id(new_data).$promise.then( function(res){
				$scope.searchList = [res]
				$scope.location = get_lng_lat(res)
				$scope.loading = false
			}).catch(function(){
				$scope.error = true
				$scope.loading = false
			})
		}

		function get_lng_lat(model){
			var posX = parseFloat((model.lat / $scope.data.by) * 100).toFixed(2),
			posY = parseFloat((model.long / $scope.data.bx) * 100).toFixed(2);
			return { x: posX + '%', y: posY + '%'};
		}

		init();
	}

	angular.module('app')
	.controller('propertiesController', ['$scope','$state','searchService', 'SEARCH',PropertiesController]);

})();