(function() {
	'use strict';

	function SearchController(
		$scope,
		$rootScope,
		searchService,
		mainFactory,
		SEARCH,
		$state,
		$stateParams
	) {
		function init() {

			$scope.searchList = []
			$scope.data = _.clone(SEARCH)
			$scope.scroll = {}
			$scope.search = null
			$scope.number_filter = [0,1,2,3,4,5]

			$scope.doSearch = doSearch
			$scope.goProperties = goProperties


			$scope.$watch("search",function(data){
				clearSearch()
				doMethod(data)
			},true)

		}

		function clearSearch (){
			$scope.searchList = []
			$scope.data.page = 1
			$scope.error = false
			$scope.no_data = false
			$scope.scroll.busy = false
		}



		function goProperties (data){
			$state.go('App.search.get',{
				id:data.id
			})
		}

		function doMethod(data){
			if (data){
				mainFactory.validate(data).then(function(res){
					var methods = {
						"search_by_id":search_by_id,
						"get_by_chars":get_by_chars
					}[res.type]
					if(methods){
						methods(res.model)
						$scope.clear_button = true
					}
				})
			}
		}

		function doSearch() {
			
			if($scope.search && !_.isEmpty($scope.search)){
				doMethod($scope.search)
			}else{
				search_list()
			}
		}

		function populate_list(data) {
			$scope.searchList = $scope.searchList.concat(data)
			$scope.scroll.busy = false
		}

		function search_list () {
			if ($scope.scroll.busy) return false
			$scope.scroll.busy = true

			searchService.get($scope.data).$promise.then( function(res){
				if (res.properties){
					$scope.data.page = ++$scope.data.page
					populate_list(res.properties)
				}else if(res.foundProperties && $scope.data.limit > res.properties.length){
					$scope.scroll.busy = true
					$scope.no_data = true
				}else{
					error_list()
				}
			}).catch(function(){
				error_list()
			})
		}

		function search_by_id (model){
			if ($scope.scroll.busy) return false
			$scope.scroll.busy = true

			var new_data = _.assign($scope.data,{id:model.id})
			searchService.get_by_id(new_data).$promise.then( function(res){
				$scope.searchList = []
				populate_list([res])
				$scope.scroll.busy = true
				$scope.no_data = true
			}).catch(function(){
				error_list()
			})
		}


		function get_by_chars (data) {
			if ($scope.scroll.busy) return false
			$scope.scroll.busy = true

			var new_data = _.cloneDeep(data,true)
			new_data = _.assign(new_data,$scope.data)
			searchService.get_by_chars(new_data).$promise.then( function(res){
				if(res.properties && res.properties.length){
					$scope.data.page = ++$scope.data.page
					populate_list(res.properties)
				}else if(res.foundProperties && $scope.data.limit > res.properties.length){
					$scope.scroll.busy = true
					$scope.no_data = true
				}else{
					error_list()

				}
			}).catch(function(){
				error_list()
			})
		}

		function error_list() {
			$scope.scroll.busy = false
			$scope.error = true
		}

		init();
	}

	angular.module('app').controller('searchController', ['$scope','$rootScope' ,'searchService', 'mainFactory','SEARCH','$state', '$stateParams', SearchController]);

})();