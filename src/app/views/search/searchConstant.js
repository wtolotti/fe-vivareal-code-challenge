(function() {
	'use strict';

	function SearchConstant(

	) {

		return {
			'ax':0,
			'ay':0,
			'bx':1000,
			'by':1400,
			'page':1,
			'limit':20
		}

	}

	angular.module('app').constant('SEARCH', SearchConstant());

})();