(function() {
	'use strict';

	function SearchService(
		$resource,
		URL_CONFIG
	) {


		var search = $resource(
			URL_CONFIG.api + "/properties/", {}, {
				get:{
					method: "GET",
					params:{
						page:'@page',
						limit:'@limit'
					}
				},
				get_by_id: {
					method: "GET",
					params: {
						id: "@id",
					},
					url: URL_CONFIG.api + "/properties/:id"
				},
				get_by_lat_lng:{
					method:"GET",
					params:{
						ax:"@ax",
						ay:"@ay",
						bx:"@bx",
						by:"@by"
					}
				},
				get_by_chars:{
					method:"GET",
					params:{
						squareMeters:'@squareMeters',
						beds:'@beds',
						baths:'@baths',
						minprice:'@minprice',
						maxprice:'@maxprice'
					}
				}
			}
		)

		return search;
	}

	angular.module('app').service('searchService', ['$resource', 'URL_CONFIG', SearchService]);

})();