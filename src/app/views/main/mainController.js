(function() {
	'use strict';

	function MainController(
		$scope,
		$location
	) {

		function init() {

			$scope.main_text = 'SPOTIPPOS - ANÚNCIOS';
			$scope.menu = [{
				value:"Anúncios",
				id:1,
				ico:'building',
				url:'search'
			},{
				value:"Novo anúncio",
				id:2,
				ico:'plus-icon',
				url:'new'
			}]

			$scope.normalize_url_path = normalize_url_path
		}

		function normalize_url_path (){ return $location.path().replace(/\//g,'')}


		init();
	}

	angular.module('app')
		.controller('mainController', ['$scope','$location', MainController]);

})();