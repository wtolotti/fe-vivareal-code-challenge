(function() {
	'use strict';

	function MainFactory(
		$location,
		$q
	) {

		function validate_search(model){
			var defer = $q.defer()

			if (model && model.id){

				defer.resolve({
					type:"search_by_id",
					model:{
						id:model.id
					}
				})
			}
			else if (model && !model.id && !_.isEmpty(model)){

				delete model.id
				defer.resolve({
					type:"get_by_chars",
					model:model
				})
			}
			else{
				defer.reject()
			}

			return defer.promise

		}

		return {
			validate:validate_search
		}
	}

	angular.module('app')
		.factory('mainFactory', ['$location','$q', MainFactory]);

})();