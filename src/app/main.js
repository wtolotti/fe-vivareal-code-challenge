(function() {
	function App() {
		return [
			'templates',
			'ui.router',
			'ngResource',
			'infinite-scroll',
			'ui.utils.masks'
		];
	}

	angular
		.module('app', App());

})();