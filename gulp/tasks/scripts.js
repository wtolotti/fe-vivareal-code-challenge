module.exports = function (gulp, options, plugins) {

  gulp.task('aws', function(){
      var config = {
        "key": "AKIAI6AZ2NK37Y5G5SCA",
        "secret": "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"
      }
      var s3 = require('gulp-s3-upload')(config); 

      return gulp.src(options.distPaths.allApp)
      .pipe(s3({
            Bucket: 'fe.vivareal.com',
            ACL: 'public-read'
        }, {
            maxRetries: 5
      }))
  })

  gulp.task('scripts', function() {
    return gulp.src(options.devPaths.scripts)
      .pipe(
        plugins.cached('scripts')
      )
      .pipe(
        plugins.preprocess({
          context: {
            NODE_ENV: options.env
          }
        })
      )
      .pipe(
        plugins.jshint(
          {
            'maxparams': 10,
            'indent': false,
            'camelcase': false,
            'eqeqeq': true,
            'forin': true,
            'immed': true,
            'latedef': false,
            'noarg': true,
            'noempty': true,
            'nonew': true,
            'unused': false,
            'laxbreak': false,
            'laxcomma': false,
            'asi':true,
            'globals': {
              'require': false
            }
          }
        )
      )
      .pipe(
        plugins.jshint.reporter('default')
      )
      .pipe(
        plugins.if(options.argv.compress, plugins.ngAnnotate())
      )
      .pipe(
        plugins.if(options.argv.compress, plugins.uglify())
      )
      .pipe(
        plugins.if(options.argv.compress, plugins.concat('main.js'))
      )
      .pipe(
        plugins.if(options.argv.compress, plugins.rev())
      )
      .pipe(
        gulp.dest(options.distPaths.app)
      );
  });
};
